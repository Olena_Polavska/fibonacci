package fibonachi;

import java.util.Scanner;

/**
     * 1 task - fibonachi.
     *
     * @author Olena Polavska
     */
class Fibonachi {
    /**
     * Choose odd numbers.
     *
     * @param number number
     * @return odd number
     */
    public static boolean odd(final int number) {
        return number % 2 != 0;
    }
    /**
     * Show odd numbers(from start to end),even numbers(from end to start).
     * Show sum odd and even numbers.
     *  @param start start of interval
     * @param finish end of interval*/

    public static void show(final int start, final int finish) {
        int sumodd = 0;
        int evensum = 0;
        try {
        System.out.println("Odd: ");
        for (int i = start; i <= finish; i++) {
            if (odd(i)) {
                System.out.println(i);
                sumodd += i;
            }
        }
        System.out.println("Sum odd: " + sumodd);

        System.out.println("Even: ");
        for (int i = finish; i >= start; i--) {
            if (!odd(i)) {
                System.out.println(i);
                evensum += i;
            }
        }
        System.out.println("Sum even: " + evensum);
        if (sumodd <= evensum) {
            System.out.println("Invalid interval");
        }
        } catch (ArithmeticException e) {
            System.exit(0);
        }
    }
    /**
     * Calculate fibonachi numbers.
     *
     * @param m how many numbers
     */
    public static void fibon(final int m) {

        int[]fibon = new int[m];
        int u = 0;
        int f = 0;
        double percent = 0;
        final int z = 100;
        System.out.println(1);
        System.out.println(1);
        for (int o = 0; o <= m; o++) {
            fibon[0] = 1;
            fibon[1] = 1;
            if (o + 2 >= m) {
                break;
            } else {
                fibon[o + 2] = fibon[o] + fibon[o + 1];
            }
            System.out.println(fibon[o + 2]);
            if (odd(fibon[o + 2]) && fibon[o + 2] == fibon[m - 1]
                    || fibon[o + 2] == fibon[m - 2]) {
                u = fibon[o + 2];
                System.out.print("Biggest odd: " + u + "\n");
            } else if (!odd(fibon[o + 2]) && fibon[o + 2] == fibon[m - 1]
                    || fibon[o + 2] == fibon[m - 2]) {
                f = fibon[o + 2];
                System.out.print("Biggest even: " + f + "\n");
            }
        }
        for (int p = 0; p <= m; p++) {
            if (p + 2 >= m) {
                break;
            } else if (odd(fibon[p + 2])) {
                percent++;
            }
        }
        percent += 2;
        percent /= (double) m;
        System.out.print("Percentage of odd numbers: " + percent * z + "%" + "\n");
        System.out.print("Percentage of even numbers: "
                + (z - (percent * z)) + "%" + "\n");

    }

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input start: ");
        int start = sc.nextInt();
        System.out.println("Input finish: ");
        int finish = sc.nextInt();
        show(start, finish);
        System.out.println("Input N: ");
        int m = sc.nextInt();
        fibon(m);
    }
}
